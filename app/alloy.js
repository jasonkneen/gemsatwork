// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

require("crux.navigator");

Alloy.Globals.fontFamily = "BebasNeueRegular";
Alloy.Globals.fontFamilyBold = "BebasNeueBold";
Alloy.Globals.fontFamilyText = "HelveticaNeue";

var cryptoJs = require("crypto-js/crypto-js");

Alloy.Globals.api = require("reste")();

// function to generate hash due to crazy-ass security model
function getHash(phrase){
    return cryptoJs.HmacSHA256(phrase + Alloy.Globals.code, "4XbLmBtjbTwQcZR8NcRdwzXjDN8vhFFL");
}

var mockData = {
    first_name: "Louise",
    surname: "Duporte",
    uid: "HOB163223",
    giveaways: [{
        title: "Optiwell Giveaway",
        slug: "optiwell-giveaway",
        start_date: "2016-02-11",
        end_date: "2016-02-20",
        opted_in: true,
        image: "http://gemsatwork.dev/wp-content/uploads/2015/11/optiwell-splash-image-bottle.png",
        delivery_day: null
    }, {
        title: "Heinz Cup Soup & Tomatina Givaway",
        slug: "heinz-cup-soup-giveaway",
        start_date: "2016-02-01",
        end_date: "2016-02-13",
        opted_in: true,
        image: "http://gemsatwork.dev/wp-content/uploads/2016/01/rsz_cream_of_tomato.jpg",
        delivery_day: null
    }],
    company: {
        title: "Hobbs Ltd",
        industry: "Retail 7 Wholesale"
    }
};



Alloy.Globals.api.config({
        timeout: 10000,
        debug: true,
        autoValidateParams: false,
        url: "https://gemsatwork.com/api/v1/",
        models: [{
            name: "user",
            id: "uid",
            read: "getUser"
        }],
        methods: [{
            name: "getUser",
            get: "ambassador/<code>",
            requestHeaders: {
                "X-Public": "EQweqFwefW£32fweRVERveq",
                "X-Hash": getHash("GET /api/v1/ambassador/") //"958c61695b5ae3ea93d22618f7f0b453cf99f0718aabdbb7bc2b9d29e67b91bd"
            },
            onLoad: function(e, callback){
                console.log(e);
                // save the user
                Alloy.Globals.user = e;
                // create the collections
                Alloy.Globals.api.createCollection("giveaways", e.giveaways);
                callback();
            }
        }],
        onLoad: function(e, callback) {
            callback(e);
        },
        onError: function(e, retry) {

            console.log(e.errors);

            if (e.errors){
                alert(e.errors[0]);
                return;
            }

            if (!e.code) {

                var dialog = Ti.UI.createAlertDialog({
                    title: "Connection problem",
                    message: "There was a network error -- check and retry.",
                    buttonNames: ["Cancel", "Retry"]
                });

                dialog.addEventListener("click", function(e) {
                    if (e.index == 1) {
                        retry();
                    } else {
                        return;
                    }
                });

                dialog.show();
            }
        }
    }

);



//Alloy.Globals.user = mockData;

// 4XbLmBtjbTwQcZR8NcRdwzXjDN8vhFFL
//console.log("2 " + cryptoJs.HmacSHA256("GET /api/v1/ambassador/BEN1253", "4XbLmBtjbTwQcZR8NcRdwzXjDN8vhFFL"));
