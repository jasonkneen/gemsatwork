// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var filter = "all";


$.tabbedBar.on("change", function(name) {
    filter = name;
    Alloy.Collections.giveaways.trigger("change");
});

function filterRows(collection) {
    return collection.where({
        reviewed: (filter == "all" || filter == "review") ? false : true
    });
}

function doLogout() {
    Alloy.App.trigger("logout");
}

function doTableClick(e) {

    Alloy.App.navigator.main.push("screens/campaign", {
        "$model": filterRows(Alloy.Collections.giveaways)[e.index]
    });

}

Alloy.Collections.giveaways.trigger("change");
