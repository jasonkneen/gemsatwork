// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

if (ENV_DEV){
    $.code.value = "BEN1253";
}

function doLogin() {

    // we have to store the code in globals so we can
    // get it for header hashing later -- crazy-ass security

    Alloy.Globals.code = $.code.value;

    // now get the user
    Alloy.Globals.api.getUser({
        code: Alloy.Globals.code
    }, function(e) {
        $.trigger("success");
    });
}
