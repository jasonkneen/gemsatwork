// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var oldTab = $.allTab

function doTab(e){
    e.source.backgroundColor = oldTab.backgroundColor;
    e.source.children[0].color = "#f9f9f9";
    oldTab.backgroundColor = null;
    oldTab.children[0].color = "#9b9b9b";
    oldTab = e.source;
    $.trigger("change", e.source.event);
}
