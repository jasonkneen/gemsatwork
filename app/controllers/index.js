function doClick(e) {
    alert($.label.text);
}

//$.index.open();

Alloy.App.on("login", function() {
    var login = Alloy.App.navigator.open("screens/login").on("success", function(callback) {
        Alloy.App.navigator.open("screens/main").on("open", function() {
            login.getView().close();
        });
    });
});


Alloy.App.on("logout", function() {

    Alloy.App.settings.set("username", null);
    Alloy.App.settings.set("password", null);

    Alloy.App.trigger("login");

    Alloy.App.navigator.main.close();
});

if (Alloy.App.settings.get("username") && Alloy.App.settings.get("password")) {
    Alloy.App.navigator.open("screens/main");
} else {
    Alloy.App.trigger("login");
}
